# Задача wrapInParagraph

```js
function wrapInParagraph(str){  
    return str.split('\n')  
        .map(row => `<p>${row}</p>`)  
        .join('\n');  
}  
  
const text = `Some  
simple multiline  
text`;  
  
const text2 = 'some\ntext';  
  
console.log(wrapInParagraph(text))
```
