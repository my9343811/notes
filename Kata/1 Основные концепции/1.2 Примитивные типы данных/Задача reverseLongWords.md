# Задача reverseLongWords 
Реализуйте функцию reverseLongWords, которая принимает строку в качестве аргумента и возвращает новую строку, в которой каждое слово, которое содержит 5 или больше символов, написана наоборот.

Примеры:

```js
reverseLongWords('Hey fellow warriors'); // Hey wollef sroirraw
reverseLongWords('This is a test'); // This is a test
reverseLongWords('This is another test'); // This is rehtona test
```

1. вариант
```js
const reverseLongWords = (str) => {  
    return str  
            .split(' ')  
            .map(word => word.length > 5 ? [...word]  
            .reverse()  
            .join('') : word)  
            .join(' ');  
};
```

2. вариант

```js

const reverseLongWords = (str) => {  
    return str  
            .split(' ')  
            .map(word => word.length > 5  
                ? word.split('').reverse().join('')  
                : word)  
            .join(' ');  
};
```

3. вариант

```js
const reverseLongWords = (str) => {  
    const result = [];  
    const strArr = str.split(' '); // Превратить строку массив  
    for (let i = 0; i < strArr.length; i++) {  
        if (strArr[i].length > 5) {  
            result.push(strArr[i].split('').reverse().join(''));  
        } else {  
            result.push(strArr[i])  
        }  
    }  
    return result;  
}
```
