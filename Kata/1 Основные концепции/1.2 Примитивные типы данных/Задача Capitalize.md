# Задача Capitalize

```js
const str1 = 'sOme RanDoM sTRING';  
  
function capitalize(str){  
    const words = [];  
    for (let word of str.split(' ')) {  
        words.push(word[0].toUpperCase() + word.slice(1).toLowerCase());  
    }  
  
    return words.join(' ');  
}  
  
console.log(capitalize(str1));
```

