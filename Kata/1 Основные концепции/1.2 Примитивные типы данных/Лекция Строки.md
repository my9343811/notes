# Строки

```js
const firstName = 'Denis';  
const lastName = 'Mescheryakov';  
const age = 30;  
const str = 'Hello my name is Denis';  
  
let value;  
  
value = firstName + ' ' + lastName;  // Denis Mescheryakov
value = value + ' I am ' + age; // Denis Mescheryakov I am 30

value = firstName.length; // 5
value = firstName[2]; // n
value = firstName[firstName.length - 1]; // s
value = firstName.toUpperCase(); // DENIS
value = firstName.concat(' ', lastName); // Denis Mescheryakov
value = firstName.indexOf('n'); // 2
value = str.indexOf('n', 10); // 19
value = str.indexOf('1'); // -1
value = str.includes('my'); // true
value = str.includes('DENIS'); // false

value = str.slice(0, 5); // Hello последний индекс не включительно
value = str.slice(0, -3); // Hello my name De

value = str.replace('Denis', 'Arthur');
```
