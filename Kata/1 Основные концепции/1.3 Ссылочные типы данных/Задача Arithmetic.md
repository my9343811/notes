# Arithmetic

Реализуйте функцию, которая принимает на вход два числа и арифметический оператор (имя которого в виде строки) и возвращает результат соответствующей операции.

Первые 2 аргумента это положительные целые числа

Третий аргумент может быть одним из "add", "subtract", "divide", "multiply".

Примеры использования:

```js
arithmetic(5, 2, "add")      => returns 7
arithmetic(5, 2, "subtract") => returns 3
arithmetic(5, 2, "multiply") => returns 10
arithmetic(5, 2, "divide")   => returns 2.5
```

В случает если оператор некорректен, функция должна возвращать NaN

```js
arithmetic(5, 2, "aaa")      => returns NaN
```


## Решение

```js

const arithmetic = (a, b, operator) => {  
    switch (operator) {  
        case 'add':  
            return a + b;  
        case 'subtract':  
            return a - b;  
        case 'multiply':  
            return a * b;  
        case 'divide':  
            return a / b;  
        default:  
            return NaN;  
    }  
};  
  
  
console.log(arithmetic(5,2,'add'))
```