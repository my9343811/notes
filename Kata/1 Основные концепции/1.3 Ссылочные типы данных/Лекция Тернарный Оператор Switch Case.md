# Тернарный оператор Switch Case

## Тернарный оператор

```js

let a = 1;  
let b = 0;  
  
if (a > 0) {  
    b = a;  
}else {  
    b += 1  
}  
  
let c = a > 0 ? b = a : b += 1;  // 1
let d = a > 0 ? 2 : a < 0 ? 3 : 0; // 2
```

## Switch Case

```js

let color = 'yellow';  
  
switch (color) {  
    case 'yellow':  
        console.log('Color is yellow');  
        break;    
    case 'red':  
        console.log('Color is red');  
        break;    
    default:  
        console.log('Default')  
}

// Color is yellow

let color = 'yellow';  
  
switch (color) {  
    case 'yellow':        
    case 'red':  
        console.log('Color is red || yellow');  
        break;    
    default:  
        console.log('Default')  
}

// Color is red || yellow
```


