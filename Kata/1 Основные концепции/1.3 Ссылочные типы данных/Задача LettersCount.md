# Задача LettersCount

Реализуйте функцию lettersCount, которая принимает строку в качестве аргумента и возвращает объект, в котором ключами являются все буквы, которые есть в строке, а значениями - их количество в строке.

Перед подсчетом буквы необходимо привести к нижнему регистру. Большая буква и маленькая должны считаться одинаковой буквой.

Пример:

```js
console.log(lettersCount('aAAbbccde'));
// {
//    a: 3,
//    b: 2,
//    c: 2,
//    d: 1,
//    e: 1,
// }
```

1.  Вариант
```js

const lettersCount = (str) => {  
    let strArr = str.split('');  
    let map = {};  
    for (let i = 0; i < strArr.length; i++) {  
        let elementArr = strArr[i].toLowerCase();  
        if (map.hasOwnProperty(elementArr)){  
            map[elementArr] = map[elementArr] + 1;  
        }else {  
            map[elementArr] = 1  
        }  
    }  
    return map;  
};
```

2.  Вариант
```js
const lettersCount = (str) => str.split('').reduce((acc, current) => (that = current.toLowerCase(), Object.assign({}, acc, acc.hasOwnProperty(that) ? {[that]: acc[that] + 1} : {[that]: 1})), {})
```

3. Вариант
```js
	const lettersCount2 = (str) => {  
    const arr = str.split('');  
    const map = {};  
    arr.forEach(item => {  
        const current = item.toLowerCase();  
        if (map.hasOwnProperty(current)) {  
            map[current] = map[current] + 1;  
        } else {  
            map[current] = 1  
        }  
    })  
  
    return map;  
}
```

4.  Вариант

```js
const lettersCount3 = (str) => (  
    map = {},  
        str.split('').forEach(item =>  
            map.hasOwnProperty(item.toLowerCase()) ?  
                map[item.toLowerCase()] = map[item.toLowerCase()] + 1  
                :  
                map[item.toLowerCase()] = 1  
        ),  
        map  
);
```
