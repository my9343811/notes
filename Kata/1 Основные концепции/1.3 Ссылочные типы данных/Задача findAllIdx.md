# Задача findAllldx

Напишите функцию findAllIdx(arr, value), которая возвращает массив индексов элементов, у которые значение равно value.

```js

function findAllIdx(arr, value) {

    return arr.map((item,index) => {

        if (item === value){

            return index;

        }

    }).filter(item => item !== undefined);

};

findAllIdx([1,0,1,0,0,1], 0) // [1,3,4] findAllIdx([1,1], 0) // []
findAllIdx([1,1], 0) // []

```

