# Рекурсия работа на js

1. 
```js

let i = 0;  
  
function func() {  
    console.log(i++);  
  
    if (i <= 10) {  
        func();  
    }  
}  
  
func();

0
1
2
3
4
5
6
7
8
9
10
```

2. 
```js

function func(arr) {  
    console.log(arr.shift());  
    if (arr.length !== 0) {  
        func(arr);  
    }  
}  
  
func([1,2,3]);

1
2
3
```

3. 
```js

function funcSum(arr, sum) {  
    sum += arr.shift();  
    if (arr.length !== 0) {  
        sum = funcSum(arr, sum);  
    }  
  
    return sum;  
}  
  
console.log(funcSum([1,2,3],0))

6
```

4. 
```js


function func(arr) {  
    for (let i = 0; i < arr.length; i++) {  
        if (typeof arr[i] == 'object') {  
            arr[i] = func(arr[i]);  
        }else {  
            arr[i] = arr[i] + '!';  
        }  
    }  
  
    return arr;  
}  
  
console.log(func([1,[2,7,8], [3, 4, [5, 6]]]));

['1!',['2!','7!','8!'], ['3!', '4!', ['5!', '6!']]]
```
