# Задача optional-chaning

Напишите функцию, которая принимает первым параметром объект, вторым - массив из цепочки свойств, по которому нужно пройти, чтобы получить значение.

Если какое-то из свойств не найдено - функция возвращает `undefined`.

```js

function optionalChaining (obj, chain) {  
  
    if (chain.length === 0 || chain.length === []) {  
        return undefined;  
    }  
    for (let prop of chain) {  
        obj = obj[prop];  
        if (obj === undefined) {  
            return undefined;  
        }  
    }  
    return obj;  
}  
  
const obj = {  
    a: {  
        b: {  
            c: {  
                d: 'Привет!'  
            }  
        }  
    }  
}  
  
console.log(optionalChaining(obj,["a", "b", "c", "d"]));  
console.log(optionalChaining(obj, ["a", "b", "c", "d", "e"]))  
console.log(optionalChaining(obj, []))
```