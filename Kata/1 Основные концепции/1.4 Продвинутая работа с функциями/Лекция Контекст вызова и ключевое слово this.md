# Контекст вызова и ключевое слово this

```js

function getThis() {  
    console.log(this)  
}  
  
function getName() {  
    console.log(this.name);  
    return this;}  
  
function getPrice(currency = '$') {  
    console.log(currency + this.price);  
    return this;}  
  
  
const prod1 = {  
    name: 'Intel',  
    price: 100,  
    getPrice,  
    getName,  
    info: {  
        information: ['2.3ghz'],  
        getInfo: function () {  
            console.log(this)  
        }  
    }  
}  
  
const prod2 = {  
    name: 'AMD',  
    price: 50,  
    getPrice,  
}  
  
const prod3 = {  
    name: 'Pentium',  
    price: 200,  
    // getPrice,  
    // getName,}  
  
prod1.getPrice(); //100  
prod1.getName(); //Intel  
prod1.info.getInfo(); // {information: Array(1), getInfo: f}  
prod2.getPrice(); //50  
  
prod2.getName = prod1.getName;  
prod2.getName() // AMD  
  
//prod3.getName().getPrice() //Pentium 200  
  
getPrice.call(prod3, 'Р') // Р200
```
	