# Замыкание Виталик

```js

function createCounter(step = 1) {  
    let number = 0; //5  
  
    return function () {  
        return number += step;  
    }  
}  
  
const counterArtur = createCounter(1);  
const counterVitalik = createCounter(5);  
  
console.group('artur')  
console.log(counterArtur()) // 1  
console.log(counterArtur())  
console.log(counterArtur())  
console.log(counterArtur())  
console.groupEnd()  
  
console.group('Vitalik')  
console.log(counterVitalik()) //5  
console.log(counterVitalik()) //10  
console.log(counterVitalik())  
console.log(counterVitalik())  
console.groupEnd()
```


```js

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 4, 5, 1, 7, 3, 11, 6, 5, 5, 15, 18];  
const arr2 = [1, 2, 3, 4, 5, 6, 7, true, undefined, NaN];  
  
const inRange = (a, b) => {  
    return (number) => {  
        if (number >= a && number <= b) {  
            return true  
        } else {  
            return false;  
        }  
    }  
};  
  
function createGetMoreNumber(referenceNumber, more = true) {  
    return (currentNumber) => {  
        return more ? currentNumber > referenceNumber : currentNumber < referenceNumber;  
    }  
}  
  
function createGetNumber(referenceNumber) {  
    return (currentNumber) => {  
        return currentNumber === referenceNumber ? true : false;  
    }  
}  
  
function createIsEven(parity = false) {  
    return (number) => {  
        const condition = parity ? number % 2 !== 0 : number % 2 === 0;  
        return condition  
    }  
}  
  
const inArray2 = arr => (elemArr) => arr.includes(elemArr);  
  
const inArray = arr => {  
    return (elemArr) => {  
        if (arr.includes(elemArr)) {  
            return true;  
        } else {  
            return false;  
        }  
    }  
};  
  
const notInArray = arr => (elemArr) => !arr.includes(elemArr);  
  
const notInArray2 = arr =>  {  
    return (elemArr) => {  
        if (arr.includes(elemArr)) {  
            return false  
        }else {  
            return true  
        }  
    }  
};  
  
const isEven = createIsEven();  
const isOdd = createIsEven(true);  
const getFive = createGetNumber(5);  
const getSeven = createGetNumber(7);  
const getMoreSix = createGetMoreNumber(6);  
const getLessThree = createGetMoreNumber(3, false);  
  
log  
console.log(arr.filter(getFive))  
console.log(arr.filter(getSeven))  
console.log(arr.filter(getMoreSix))  
console.log(arr.filter(getLessThree))  
console.log(arr.filter(isOdd))  
console.log(arr.filter(isEven))  
  
console.log(arr2.filter(inRange(3, 6)))  
console.log(arr2.filter(inArray([1, 2, 10, undefined])));  
console.log(arr2.filter(notInArray([1, 2, 3, 4, 5, 6, 7, true])));
```
