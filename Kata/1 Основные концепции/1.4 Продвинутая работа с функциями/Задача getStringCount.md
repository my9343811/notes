# getStringCount

Реализуйте (с использованием рекурсии) функцию getStringCount, которая должна принимать массив или объект и считать количество строк в массиве / значениях объекта с учетом вложенности.

```js
getStringCount({   first: '1',   second: '2',   third: false,   fourth: ['anytime', 2, 3, 4 ],   fifth:  null, }); // 3
getStringCount(['1', '2', ['3']]) // 3
```

```js

function getStringCount(object) {  
    let count = 0;  
  
    if (Array.isArray(object)) {  
        for (let item of object) {  
            count += getStringCount(item);  
        }  
    } else if (typeof object === 'object' && object !== null) {  
        for (let key in object) {  
            count += getStringCount(object[key]);  
        }  
    } else if (typeof object === 'string') {  
        count++;  
    }  
  
    return count;  
}
```



