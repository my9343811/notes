# Стрелочные функции

```js

const plus = (x,y) => x + y;  
  
console.log(plus(4,5)) // 9  
  
const withoutArgs = () => console.log('Hello world!');  
const singleArg = x => x * 2;  
const moreActions = (a, b) => {  
    a *= 2;  
    b *= 3;  
    return a + b;  
}  
console.log(singleArg(3)) // 6  
  
function plusFoo(x,y) {  
    console.log(arguments)  
    return x + y;  
}  
  
plusFoo(1,2,3, 'hello') // Arguments(4) [1, 2, 3, 'hello', callee: ƒ, Symbol(Symbol.iterator): ƒ]
```
