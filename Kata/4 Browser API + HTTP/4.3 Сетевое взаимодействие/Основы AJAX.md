# Основы AJAX

1. Создаём новый объект XMLHttpRequest
```js
const xhr = new XMLHttpRequest();
```
2. Конфигурируем его: GET-запрос на URL 'https://jsonplaceholder.typicode.com/posts' т.е. настройки запроса
```js
xhr.open('GET', 'https://jsonplaceholder.typicode.com/posts');
```

3. Если успешно произошло запрос, то подписываемся на события загрузки и получения данных от сервера
```js

xhr.addEventListener('load', () => {  
    console.log(xhr.responseText);  
})
```

4. отсылаем запрос
```js
xhr.send();
```

5. Можно обрабатывать ошибки подписавшись на событие error
```js

xhr.addEventListener('error', () => {  
    console.log('error')  
})
```
## Практика

```js

const btn = document.querySelector('button');  
const container = document.querySelector('.container');

function getPosts(callback) {  
    const xhr = new XMLHttpRequest();  
    xhr.open('GET', 'https://jsonplaceholder.typicode.com/posts');  
    xhr.addEventListener('load', () => {  
        const response = JSON.parse(xhr.responseText);  
        callback(response)  
    })  
  
    xhr.addEventListener('error', () => {  
        console.log('error')  
    })  
  
    xhr.send();  
}  
  
btn.addEventListener('click', e => {  
    getPosts((response) => {  
        const fragment = document.createDocumentFragment();  
        response.forEach(post => {  
            const card = document.createElement('div');  
            card.classList.add('card');  
  
            const cartBody = document.createElement('div');  
            cartBody.classList.add('cartBody');  
            const title = document.createElement('h5');  
            title.classList.add('cart-title');  
            title.textContent = post.title  
  
            const article = document.createElement('p');  
            article.classList.add('cart-text');  
            article.textContent = post.body;  
  
            cartBody.appendChild(title);  
            cartBody.appendChild(article);  
            card.appendChild(cartBody);  
            fragment.appendChild(card);  
        })  
        container.appendChild(fragment);  
    })  
})
```



