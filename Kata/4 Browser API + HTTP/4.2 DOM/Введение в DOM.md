# Введение в DOM
1. 
```js

const div = document.querySelector('.content');
console.log(div);
// <div class="content"></div>

const titles = document.querySelectorAll('h1');
console.log(titles);
//NodeList(2) [h1, h1]
```

>[!info]
>Оператором spread можно получить массив console.log([...titles]);


2. Получить элементы по имени тега
```js

const h1 = document.getElementsByTagName('h1');  
console.log(h1)

//HTMLCollection(2) [h1, h1]
```





