# Работа с атрибутами

```html
<div class="main">  
    <h1>Заголовок 1</h1>  
    <h1>Заголовок 2</h1>  
    <div class="content">  
        Оператором spread можно получить массив console.log([...titles]);  
    </div>  
    <div class="text">  
        Текст текст  
    </div>  
</div>
```
1. parentElement ищет родительский класс
```js

const div = document.querySelector('.text');
console.log(div.parentElement)
// <div class="main"></div>
```

2. Добавить класс в тег
```js

const div = document.querySelector('.text');  
div.classList.add('article', 'custom');

<div class="text article custom">
            Текст текст
</div>
```

3. Удаление класса 
```js

const div = document.querySelector('.text');  
div.classList.remove('article')
```

4. Существует ли класс
```html
<div class="text custom"></div>
```

```js

console.log(div.classList.contains('custom'))
// true
```

5. Класс toggle
```js

div.classList.toggle('toggle')
//true
div.classList.toggle('toggle')
//false
```

6. setAttribute
```js
const span = document.querySelector('span');  
span.setAttribute("id", 'myId')
```
>[!warning]
>и получим

```html
И получим <span id = "myId">Span</span>
```



