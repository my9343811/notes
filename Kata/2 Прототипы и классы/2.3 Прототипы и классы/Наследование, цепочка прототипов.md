# Наследование, цепочка прототипов

```js

function Auto(brand,price,gas) {  
    this.brand = brand;  
    this.price = price;  
    this.gas = gas;  
}  
  
Auto.prototype.drive = function () {  
    if (this.gas > 0) {  
        this.gas = this.gas - 20;  
        return this.gas;  
    } else {  
        console.log('Бензин закончился');  
    }  
}  
  
const bmw = new Auto('bmw', '100 000', 100);  
const nissan = new Auto('nissan', '40 000', 100);  
  
console.log(bmw.drive())  
console.log(bmw.drive())  
console.log(bmw.drive())  
console.log(bmw.drive())  
console.log(bmw.drive())  
console.log(bmw.drive())  
console.log(nissan.drive())  
console.log(nissan.drive())
```


