# Set

>[!info]
>Объект Set – это особый вид коллекции: «множество» значений (без ключей), где каждое значение может появляться только один раз.

1. 
```js

const set = new Set(['button', 'active','small']);  
  
console.log(set)

Set(3) {'button', 'active', 'small'}
```

2.  заменим на строку
```js

const set = new Set('button');  
  
console.log(set)

Set(5) {'b', 'u', 't', 'o', 'n'}
```

3. не работает с числами
```js

const set = new Set(42);  
  
console.log(set)
// Покажет ошибку number 42 is not iterable
```

>[!error]
>Конструктор Seta пытается вызвать специальный метод у аргумента для получения перебираемого объекта. У чисел нет такого метода и поэтому получаем ошибку

4. 
```js

const set = new Set();  
  
set.add('button')  
set.add('active')  
set.add('button')  

// по цепочке тоже самое
set.add('button')  
   .add('active')  
   .add('button') 
  
console.log(set)

Set(2) {'button', 'active'}

console.log(set.size) // 2
```

5. has
```js

const set = new Set();  
  
set.add('button')  
    .add('active')  
    .add('button')  
  
console.log(set.has('active'))  // true
console.log(set.has('primary')) // false
```

## Работа Set с объектами

1. Разные объекты по этому не равны

```js

const set = new Set();  
  
set.add({className: 'button'})  
  
console.log(set.has({className: 'button'})) //false
```

2. Объекты равны

```js

const set = new Set();  
const buttonRef = {className: 'button'}  
  
set.add(buttonRef)  
  
console.log(set.has(buttonRef)) // false
```

3. Удаление значение Seta

```js

const set = new Set();  
  
set.add('button')  
    .add('active')  
    .add('link')  
  
set.delete('link')  
console.log(set)

Set(2) {'button', 'active'}
```

4. Clear очистить все
```js
const set = new Set();  
  
set.add('button')  
    .add('active')  
    .add('link')  
set.clear()

Set(0) {size: 0}
```

5. 
```js

const set = new Set();  
  
set.add('button')  
    .add('active')  
    .add('link')  
  
  
console.log(set.values()) // SetIterator {'button', 'active', 'link'}
console.log(...set.values()) // button active link
console.log(...set.keys()) // button active link

console.log([...set.entries()]) // [['button', 'button'],['active','active'],['link','link']]

console.log([...set]) // ['button', 'active', 'link']
console.log(Array.from(set)) // ['button', 'active', 'link']
```

>[!warning]
>В Sete нет понятия индекс

6. Перебор циклом for
```js

const set = new Set();  
  
set.add('button')  
    .add('active')  
    .add('link')  
  
  
for (let item of set) {  
    console.log(item)  
}

button
active
link
```



