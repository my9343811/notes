# Map
1. 
```js

const map = new Map([  
    ['Hi', 'Привет'],  
    [42, 'Всем хорошего настроения'],  
    [true, false],  
    [{},'Объект'],  
    [function () {}, 'Функция']  
]);  
  
console.log(map);

0: {"Hi" => "Привет"}
1: {42 => "Всем хорошего настроения"}
2: {true => false}
3: {Object => "Объект"}
4: {function () {} => "Функция"}
```

## Установить значение set

2. 
```js

const map = new Map();  
  
map.set(42, 'Какая то строка'); // добавляем значение
map.set('42', 'Какая то строка')  
console.log(map)

console.log(map.get(42)) // get получить значение по ключу
console.log(map.get(44)) // если нет ключа то вернет undefined

console.log(map.has(42)) // true
console.log(map.has(43)) // false

map.delete(42); // Удаление по ключу
//{'42' => 'Какая то строка'}

map.clear(); // Очистить все значение
```

3. 
```js

const map = new Map();  
  
map.set('HTML', 'HyperText Markup Language')  
    .set('CSS', 'Cascading Style Sheets')  
    .set('JS', 'Javascript');  
  
console.log(map.keys()) //MapIterator {'HTML', 'CSS', 'JS'}
console.log(...map.keys()) // HTML CSS JS
console.log([...map.keys()]) // ['HTML', 'CSS', 'JS']

console.log(map.values()) // MapIterator {'HyperText Markup Language', 'Cascading Style Sheets', 'Javascript'}
console.log([...map.values()]) // ['HyperText Markup Language', 'Cascading Style Sheets', 'Javascript']


```

4. 
```js

const map = new Map();  
  
map.set('HTML', 'HyperText Markup Language')  
    .set('CSS', 'Cascading Style Sheets')  
    .set('JS', 'Javascript');  
  
console.log([...map.entries()])

// [
//  ['HTML', 'HyperText Markup Language']
//  ['CSS', 'Cascading Style Sheets']
//  ['JS', 'Javascript']
// ]
```

5. 
```js
const map = new Map();  
  
map.set('HTML', 'HyperText Markup Language')  
    .set('CSS', 'Cascading Style Sheets')  
    .set('JS', 'Javascript');  
  
const [first, second] = map;  
  
console.log(second);//['CSS', 'Cascading Style Sheets']
```

6. 
```js
const map = new Map();  
  
map.set('HTML', 'HyperText Markup Language')  
    .set('CSS', 'Cascading Style Sheets')  
    .set('JS', 'Javascript');  
  
const [[key, value], second, third] = map;  
  
console.log(key, value) // HTML HyperText Markup Language
```
