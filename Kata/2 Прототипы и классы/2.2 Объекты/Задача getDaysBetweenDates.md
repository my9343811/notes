# Задача getDaysBetweenDates

Реализуйте функцию `getDaysBetweenDates` которая принимает на вход две даты и возвращает количество полных дней между ними.

`getDaysBetweenDates('1-1-2020', '1-2-2020'); // -> 1`

- Функция должна корректно работать с объектом Date

`getDaysBetweenDates(new Date(2011, 6, 2, 6, 0), new Date(2012, 6, 2, 18, 0)); // -> 366`

- Функция должна корректно рабоать со значениями в миллисекундах

`getDaysBetweenDates(1409796000000, 1409925600000); // -> 1`

- Если входные параметры - невалидные даты, то функция вовращает NaN:

`getDaysBetweenDates('1-1-2020', 'дата'); // -> NaN`

- Если аргументов меньше 2-х, то функция должна пробросить исключение TypeError

`getDaysBetweenDates(null); // -> TypeError`

> new Date(null) - валидная запись, которая вернёт количество миллисекунд, прошедшее с 01.01.1970 [https://en.wikipedia.org/wiki/Unix_time](https://en.wikipedia.org/wiki/Unix_time "https://en.wikipedia.org/wiki/Unix_time")


```js

function getDaysBetweenDates(a, b) {

  if (arguments.length < 2) {

    throw new TypeError("TypeError");

  }

  let date1 = new Date(a);

  let date2 = new Date(b);

  

  if (date1 === 'Invalid Date' || date1 === 'Invalid Date') return NaN;

  let oneDay = 1000 * 60 * 60 * 24;

  let time = date2.getTime() - date1.getTime();

  let days = 0;

  

  days = Math.trunc(time / oneDay);

  if (days === -0) days = 0

  

  return days;

  

};
```