# Объект Date

```js

const time1 = new Date();  
console.log(time1)  
// Tue Apr 09 2024 17:09:10 GMT+0500 (Екатеринбург, стандартное время)  
const time2 = new Date(2024, 3, 9,17,16);  
console.log(time2)  
const time3 = new Date('2024 May 11');  
console.log(time3)  
let time4 = new Date('2-28-2024')  
console.log(time4)  
  
const date = new Date();  
console.log(date.getFullYear()) // 2024  
console.log(date.getMonth()) // месяц начинается с 0 и до 11  
console.log(date.getDate()) // текущая дата 1..31  
console.log(date.getDay()) // текущий день недели начиная с Воскресенье 0..6
```