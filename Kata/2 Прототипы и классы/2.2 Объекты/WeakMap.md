# WeakMap

>[!info]
>С помощью WeakMap мы можем избегать различных утечки данных в JS

>[!warning]
>В структуре WeakMap ключами могут являться только объекты

1. ссылка на объект сохраняется
```js

let obj = {name: 'WeakMap'};  
  
const arr = [obj];  
  
obj = null;  
  
console.log(obj)  //null
console.log(arr) // [{name: 'WeakMap'}]
```

2.  очищается память
```js

let obj = {name: 'WeakMap'};  
  
const map = new WeakMap([  
    [obj, 'Data Object Model']  
]);  
  
obj = null;  
  
  
console.log(map);
```
