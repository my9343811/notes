# Операторы Spread и Rest

## Spread 
1. с Массивом
```js

const citiesOfRussia = ['Москва', 'Санкт-Петербург', 'Уфа', 'Краснодар'];  
const citiesOfEurope = ['Берлин', 'Прага', 'Рим', 'Париж'];  
  
// Spread  
const allCities = [...citiesOfRussia, 'Акъяр', ...citiesOfEurope];  
  
console.log(allCities)

['Москва', 'Санкт-Петербург', 'Уфа', 'Краснодар', 'Акъяр', 'Берлин', 'Прага', 'Рим', 'Париж']
```

2. с Объектами

```js

const citiesRussiaWithPopulation = {  
    Moscow: 10,  
    SaintPetersburg: 3,  
    Kazan: 2,  
    Novosibirsk: 3,  
}  
const citiesEuropeWithPopulation = {  
    Moscow: 26,  
    Berlin: 10,  
    Prague: 3,  
    Paris: 2,  
}  
  
console.log({...citiesRussiaWithPopulation, ...citiesEuropeWithPopulation})

{Moscow: 26,SaintPetersburg: 3,Kazan: 2,Novosibirsk: 3,Berlin: 10, Prague: 3, Paris: 2,}
```

## Rest

1. 
```js

function sum(a,b, ...rest) {  
    console.log(rest)  
    return a + b;  
}  
  
const numbers = [1,2,3,4,5];  
console.log(sum(...numbers));

3 [3,4,5]
```

2. 
```js

function sum(a,b, ...rest) {  
    return a + b + rest.reduce((acc,item) => item + acc,0);  
}  
  
const numbers = [1,2,3,4,5];  
console.log(sum(...numbers)) // 15
```

