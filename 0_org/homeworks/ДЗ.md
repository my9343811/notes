# ДЗ 1
	40% проекта
1. Promise.all() - понять что это такое и как правильно пользоваться
2. fetch(url, options) - понять что такое options и как им пользоваться и зачем он нужен. Научиться делать запросы используя options(POST,DELETE,PUT,PATCH)
3. Создать services (https://front-test.dev.aviasales.ru/search и https://front-test.dev.aviasales.ru/tickets). Используя простой клиент
4. Создать store и подключить slice(list [{}])

# ДЗ 2
1. Разобрать reduce (Метод массива)
2. Повторить sort, filter, 
3. Научиться самому показывать по 5 билетов
4. Научиться загружать без extraReducers(Обработка ошибки, обработка загрузки)
5. Повторить все утилиты в aviasales
6. Проверить Aviasales
### Задачи по reduce
1. Преобразовать массив объектов в объект объектов по id
2. Найти сумму массива объектов такого вида {price: 100, count: 5} (price * count):
```javascript
const items = [
  { price: 100, count: 5 },
  { price: 200, count: 3 },
  { price: 150, count: 2 }
];
```
3. Найти максимальное значение в массиве чисел:
```javascript
const numbers = [10, 20, 30, 40, 50];
```

# ДЗ 2.1
1. Повторить метод reduce()
2. И повторить математику с временем 
3. Сделать учетную запись Microsoft whiteboard


# ДЗ 3

1. Научиться делать сортировку 
2. Научиться использовать reselect(createSelector)
3. Разобрать новые utils Aviasales
