# заголовок первого уровня (h1)
## заголовок первого уровня (h2)
### заголовок первого уровня (h3)
#### заголовок первого уровня (h4)
##### заголовок первого уровня (h5)
###### заголовок первого уровня (h6)

## Списки
### Нумерованный

1. первый
2. второй
3. третий

### Декоративный

- item 1
- item 2
- item 3

### Вложенный

1. Именованный
	- Маркированный
	-  не маркированный
2. не именованный
## Код 


### JS

```js
function addTask(title: string) {  
    const newTask = {id: v1(), title, isDone: false};  
    const newTasks = [newTask, ...tasks];  
    setTasks(newTasks);  
}
```
### css
```css
.App > div {  
  margin-right: 50px;  
}  
  
.error {  
  border: red 1px solid;  
}  
  
.error-message {  
  color: red;  
}  
  
.active-filter {  
  background-color: chartreuse;  
}  
  
.is-done {  
  opacity: 0.5;  
}
```

### Html
```html
<div class="container">  
    <h3>Исходный массив</h3>  
    <div class="out-1-source"></div>  
    <div class="out-1-source-length"></div>  
    <div class="out-1-source-image array"></div>  
    <h3>Добавить элемент</h3>  
    <select class="array-element">  
        <option value="worm">worm</option>  
        <option value="dog">dog</option>  
        <option value="snake">snake</option>  
        <option value="seastar">seastar</option>  
        <option value="ryno">ryno</option>  
        <option value="pinguin">pinguin</option>  
        <option value="mouse">mouse</option>  
        <option value="hedgehod">hedgehod</option>  
        <option value="giraffe">giraffe</option>  
        <option value="frog">frog</option>  
        <option value="crab">crab</option>  
    </select>    <span>Индекс:</span>  
    <input class="array-index" type="number" value="3">  
    <button class="add-to-array">Добавить</button>  
</div>
```

### Git 

```shell
git status
```


## Text
простой текст
__жирный текст__
_курсив_
==выделенный текст==
`git status`


## callouts

> [!warning] важно
> marginы могут схлопываться



> [!info] Информация


> [!abstract]

> [!example]

> [!Error]

## Ссылка


[[info]]
[[README]]


![[webstorm]]

### Images

![[crab.png]]![[crab.png]]

## Линия
***
