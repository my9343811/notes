## первая группа (шанс больше 60%)

- Что такое JWT?
- Все что касается HTTP
- cookie
- Critical path render
- Что происходит, когда ты набираешь адрес в адресной строке и нажимаешь Enter?
- DOM
- sessionStorage и localStorage?
- API
- REST
- HOST,Domain,URL
- CDN
- Что такое блокирующие и неблокирующие ресурсы?
- script defer, script, async script

## вторая группа (шанс около 50% или меньше)

- Безопасные и идемпотентные методы?
- Что такое CORS?
- Что такое Content Security Policy (CSP)?
- Что такое XSS?
- TCP/UDP
- TCp/IP
- Web Sockets
- GraphQL

