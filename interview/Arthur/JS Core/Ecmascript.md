## Что такое ECMAScript? В чём отличие от JavaScript?

ECMAScript - это спецификация, стандарт, определяющий язык программирования JavaScript. Он описывает синтаксис, типы данных, операторы, объекты и функции языка JavaScript. ECMAScript был создан для обеспечения единого стандарта для разработчиков языка, и JavaScript это одна из реализация стандарта. На данный момент последней версией стандарта является ECMAScript 2023.

## Можно ли изменить значение определённое через const?

Свойства и методы объекта объявленные через const могут быть изменены. Т.к. хранится ссылка, а не сами данные, сохраняя ссылку данные можно поменять.

## Что такое временная мёртвая зона (temporal dead zone)?

Временная мертвая зона (Temporal Dead Zone) – это состояние, в котором переменные недоступны. Они находятся в области видимости (scope), но не объявлены (not declared).

Переменные объявленные через **`let` и  `const` существуют в области TDZ от начала их в области видимости до момента их декларирования (объявления).

```js
console.log(a) // ReferenceError: a is not defined

let a = 10;
```

