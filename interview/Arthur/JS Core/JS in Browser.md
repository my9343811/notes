# JS in Browser

## Что такое DOM?

DOM (Document Object Model) - это объектная модель документа, которую браузер создает в памяти компьютера на основании HTML кода полученного им от сервера. С помощью DOM, JavaScript может взаимодействовать с элементами страницы.

- document - объект, представляющий дерево DOM (точка входа в DOM)
- узлы-элементы - теги HTML-документа
- текстовые узлы - элементы, содержащие текстовую информацию
- комментарии - элементы с комментариями

## Типы узлов DOM-дерева?


 
## Методы поиска элементов в DOM?

- getElementById()
- getElementsByName()
- getElementsByTagName()
- getElementsByClassName()
- querySelector()
- querySelectorAll()


## Свойства для перемещения по DOM-дереву?



## Что такое BOM?

BOM объектная модель браузера(Browser Object Model) - дополнительные объекты, предоставляемые браузером.

## Что такое распространение события (Event Propagation)?

Распространение события (Event Propagation) — это механизм в веб-разработке, который описывает, как события, такие как клики мыши или нажатия клавиш, перемещаются по иерархии DOM (Document Object Model). Все это делится на 3 фазы:
1. **Погружения (Capturing)** событие сначала идет сверху вниз.
2. **Target** событие достигает целевого элемента.
3. **Всплытие (Bubbling)** фаза начинает всплывать, параллельно вызывая все события на родительских элементах.

![[Pasted image 20250210205300.png]]
## Что такое делегирование событий (Event Delegation)?







1) Без делегирование
```html
<div class="todo">
   <ul class="todo__list">
     <li class="todo__item">Todo item</li>
     <li class="todo__item">Todo item</li>
     <li class="todo__item">Todo item</li>
     <li class="todo__item">Todo item</li>
     <li class="todo__item">Todo item</li>
     <li class="todo__item">Todo item</li>
   </ul>
</div>
```

```js
const todoItemElements = document.querySelectorAll('.todo__item');
todoItemElements.forEach(todoItemElem => {
    todoItemElem.addEventListener('click', () => {
        todoItemElem.classList.add('is-completed')
    })
})
const addTodoItem = () => {
    const todoListElement = document.querySelector('.todo__list');
    const newTodoItemMarkUp = `<li class="todo__item">Todo item</li>`;
    todoListElement.insertAdjacentHTML('beforeend', newTodoItemMarkUp)
}

addTodoItem()
addTodoItem()
addTodoItem()
```
1) Делегирование
```js
const onTodoItemClick = (todoItemElement) => {
    todoItemElement.classList.add("is-completed");
}

document.addEventListener('click', (event) => {
    if (event.target.classList.contains('todo__item')) {
        onTodoItemClick(event.target)
    }
})

const addTodoItem = () => {
    const todoListElement = document.querySelector('.todo__list');
    const newTodoItemMarkUp = `<li class="todo__item">Todo item</li>`;
    todoListElement.insertAdjacentHTML('beforeend', newTodoItemMarkUp)
}

addTodoItem()
addTodoItem()
addTodoItem()
```

## Разница между HTMLCollection и NodeList?

## Чем отличается querySelector и getElementByClassName?

## Разница между e.preventDefault() и e.stopPropagation()?

## Разница между event.target и event.currentTarget?


## Разница между событиями load и DOMContentLoaded?
