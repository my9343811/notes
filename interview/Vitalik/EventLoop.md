## Event Loop

**Event Loop** - это бесконечный цикл событий, который управляет порядком выполнения задач.

**Event Loop** - не является частью языка JavaScript

После выполнения всех `Micro Task` Event Loop смотрит в `Render queue` (задачи на отрисовку), а затем в `Macro Task queue`.

**Приоритет выполнения:**

- Синхронные задачи
- Micro tasks
- Одна Macro task

- Сначала выполняются все синхронные задачи.
- После выполняются все micro tasks.
- После выполняется одна macro task. Если она порождает micro task, то сначала выполнится micro task, а после цикл повторится.

**Цикл Event Loop:**

- Когда `call stack` пуст, мы проверяем, есть ли что-то в `Micro Task queue`.
- Если в `Micro Task queue` что-то есть, то мы берем первую задачу и помещаем её для выполнения в `call stack`.
- Выполняя задачу в `call stack`, она может породить синхронную задачу, которая также попадет в `call stack` и выполнится перед родительской задачей.
- Выполняя задачу в `call stack`, она может породить асинхронную задачу. Асинхронная задача может быть двух типов: `Micro Task` (`then`, `catch`, `finally`) и `Macro Task` (`setTimeout`, `addEventListener`). Для каждого типа есть своя очередь.
- После выполнения всех задач в `Micro Task queue` проверяется `Macro Task queue`. Берется первая задача из `Macro Task queue`, помещается в `call stack` и выполняется. Если мы породили `Micro Task`, то сначала будут выполняться она, прежде чем мы сможем перейти к следующей `Macro Task`.
- Event Loop бегает по кругу, проверяя `call stack`, `Micro Task queue` и `Macro Task queue`.

## Call Stack (главный поток)

У нас есть `call stack`, в который попадают все задачи для выполнения. `Call stack` работает по принципу "первым зашел - последним вышел" (Last In First Out, `LIFO`). Это значит, что последний элемент, добавленный в стек, будет взят из него первым. `Call stack` в JavaScript не бесконечный и может переполниться, например, если будет большая глубина рекурсии.

## Micro Task Queue

Иногда называю как очередь задач

- `Promise`
- `queueMicrotask`
- `mutationObserver`

## Macro Task Queue

Иногда называю как очередь событий

- `setTimeout`
- `setInterval`
- `addEventListener`

## Render Queue

- `requestAnimationFrame` - вызывается перед отрисовкой следующего кадра

## Web API

- `fetch`
- `XMLHttpRequest`
- `setTimeout`
- `localStorage`
- `document`

## Пример 1

```javascript
setTimeout(() => {
  console.log(1);
}, 1000);
```

- Изначально `setTimeout` попадет в `call stack`, но выполниться он не может, так как нужно подождать секунду. Поэтому он перемещается в `web api`, где будет ждать одну секунду.
    
- После `web api`, `console.log(1);` попадет в `Macro Task queue`, где будет ждать завершения выполнения `call stack` и `Micro Task queue`, если там что-то есть.
    
- Когда `call stack` освободится, `console.log(1);` попадет в `call stack` и выполнится.

## Пример 2

```js
const button = document.querySelector('#btn');

button.addEventListener('click', () => {
  console.log('click');
});
```

- Изначально `addEventListener` попадет в `call stack`, но за регистрацию события отвечает `web api`, поэтому он переедет туда. В `web api` событие будет зарегистрировано и будет ожидать, когда пользователь нажмет кнопку.
    
- Как только пользователь нажмет кнопку, задача с `callback` попадет в `Macro Task queue`.
    
- Когда `call stack` освободится, `callback` попадет в `call stack` и выполнится.

## Easy

### Easy Task 1


```js
setTimeout(function timeout() {
  console.log('1');
}, 0);

let p = new Promise(function (resolve, reject) {
  console.log('2');
  resolve();
});

p.then(function () {
  console.log('3');
});

console.log('4');

// 2 4 3 1
```

### Easy Task 2

```js

console.log(1);
setTimeout(() => console.log(2));
Promise.resolve().then(() => console.log(3));
Promise.resolve().then(() => setTimeout(() => console.log(4)));
Promise.resolve().then(() => console.log(5));
setTimeout(() => console.log(6));
console.log(7);

// 1 7 3 5 2 6 4
```


### Easy Task 3

```js
const promise = new Promise(() => {
  console.log(1);
});

promise.then((data) => {
  console.log(2);
  console.log(data);
});

Promise.resolve(3).then(console.log);

// 1 3
```

## Hard

### Hard Task 1


```js
console.log(1);
setTimeout(() => console.log(2));
Promise.reject(3).catch(console.log);
new Promise((resolve) => setTimeout(resolve)).then(() => console.log(4));
Promise.resolve(5).then(console.log);
console.log(6);
setTimeout(() => console.log(7), 0);

// 1 6 3 5 2 4 7
```

сложный момент в том что 

```js
new Promise((resolve) => setTimeout(resolve)).then(() => console.log(4));
```

`setTimeout(resolve)` добавится в macro task синхронно потому функция переданная в Promise выполняется синхронно.



### Hard Task 2

```js
const myPromise = (delay) =>
  new Promise((res, rej) => {
    setTimeout(res, delay);
  });

setTimeout(() => console.log('in setTimeout1'), 1000);
myPromise(1000).then((res) => console.log('in Promise 1'));
setTimeout(() => console.log('in setTimeout2'), 100);
myPromise(2000).then((res) => console.log('in Promise 2'));
setTimeout(() => console.log('in setTimeout3'), 2000);
myPromise(1000).then((res) => console.log('in Promise 3'));
setTimeout(() => console.log('in setTimeout4'), 1000);
myPromise(5000).then((res) => console.log('in Promise '));

// in setTimeout2
// in setTimeout1
// in Promise 1
// in Promise 3
// in setTimeout4
// in Promise 2
// in setTimeout3
// in Promise
```


### Hard Task 3

```js
console.log('Синхронный старт');

// макрозадача с setTimeout
setTimeout(() => {
  console.log('Макрозадача 1: setTimeout');
}, 0);

// Макрозадача с setTimeout
setTimeout(() => {
  console.log('Макрозадача 2: setTimeout');
}, 0);

// Микрозадача с Promise
Promise.resolve()
  .then(() => {
    console.log('Микрозадача 1: выполнение первого промиса');
  })
  .then(() => {
    console.log('Микрозадача 2: выполнение второго промиса');
  });

requestAnimationFrame(() => {
  console.log('Первый requestAnimationFrame: обновление анимации');
});

requestAnimationFrame(() => {
  console.log('Второй requestAnimationFrame: обновление анимации');
});

console.log('Синхронный конец');
```

`requestAnimationFrame` может обработаться по разному в зависимости от тайменга рендера

__Start1:__

```js
// Синхронный старт
// Синхронный конец
// Микрозадача 1: выполнение первого промиса
// Микрозадача 2: выполнение второго промиса
// Макрозадача 1: setTimeout
// Первый requestAnimationFrame: обновление анимации
// Второй requestAnimationFrame: обновление анимации
// Макрозадача 2: setTimeout
```

__Start2:__

```js
// Синхронный старт
// Синхронный конец
// Микрозадача 1: выполнение первого промиса
// Микрозадача 2: выполнение второго промиса
// Первый requestAnimationFrame: обновление анимации
// Второй requestAnimationFrame: обновление анимации
// Макрозадача 1: setTimeout
// Макрозадача 2: setTimeout
```


## Блокировка Event loop
### Task 1 👿

```js
let count = 0;

console.log('start');

function func(promise) {
  console.log('func');

  promise.then(() => {
    console.log('promise.then');

    if (count < 3) {
      ++count;
      func(Promise.resolve());
    }
  });
}

func(Promise.resolve());
setTimeout(() => console.log('setTimeout'), 0);

console.log('end');
```

```js
// start
// func
// end
// promise.then
// func
// promise.then
// func
// promise.then
// func
// promise.then
// setTimeout
```


### Task 2

```js
console.log('start');

for (let i = 0; i < 5; i++) {
  console.log(i);
  Promise.resolve().then(() => console.log(i));
}

console.log('end');

setTimeout(() => console.log('Timeout'));
```

```js
// start
// 0
// 1
// 2
// 3
// 4
// end
// 0
// 1
// 2
// 3
// 4
// Timeout
```