# Questions HTML 

## Общее

### Что такое Html для чего он используется ?
HTML (HyperText Markup Language) — это стандартный язык разметки для создания веб-страниц и веб-приложений. Вот для чего он используется:

1. **Структурирование веб-контента**: HTML позволяет структурировать текст, изображения и мультимедийный контент на веб-страницах с использованием различных тегов.
    
2. **Создание гиперссылок**: С помощью HTML можно создавать гиперссылки, которые позволяют пользователям переходить с одной страницы на другую.
    
3. **Формы и взаимодействие с пользователем**: HTML используется для создания форм, которые позволяют пользователям вводить данные и отправлять их на сервер для обработки.
    
4. **Встраивание мультимедиа**: HTML поддерживает встраивание видео, аудио и других мультимедийных элементов на веб-страницы.

### Что такое валидация? Как проверить валидацию Html?

Валидация (англ. validation) – это процесс проверки данных на соответствие определённым правилам или стандартам. В контексте HTML, валидация означает проверку HTML-кода на соответствие стандартам и спецификациям, установленным организацией W3C (World Wide Web Consortium). Валидация помогает обнаружить ошибки в коде, которые могут повлиять на корректное отображение веб-страницы в браузерах.

## Атрибуты
###  Что такое инлайновый стиль? Как его переопределить?


Единственный способ переопределить такие стили это !important

### Что такое data атрибуты?

Data-атрибуты (или пользовательские атрибуты) в HTML используются для хранения дополнительных данных на элементах HTML без необходимости использовать глобальные атрибуты или другие способы хранения данных, такие как JavaScript. Они могут быть полезны для хранения информации, связанной с элементами, которую можно легко получить с помощью JavaScript.

```html
<div data-user-id="12345" data-role="admin">...</div>
```
В данном примере `div` элемент имеет два data-атрибута: `data-user-id` и `data-role`. Эти атрибуты можно использовать для хранения информации о пользователе и его роли.

```js
var div = document.querySelector('div');
var userId = div.getAttribute('data-user-id');
var role = div.getAttribute('data-role');

console.log(userId); // Выведет "12345"
console.log(role); // Выведет "admin"
```

### Зачем нужен tabindex?
Атрибут tabindex в HTML используется для управления порядком фокуса при навигации по элементам страницы с помощью клавиши Tab. Он позволяет определить, какие элементы могут получать фокус, и в каком порядке это происходит.

###  Разница между`<script>`, `<script async>`и `<script defer>`?
- `<script>`:
    
    - **Стандартный сценарий**. Браузер выполняет его сразу же при загрузке документа.
        
    - Это блокирует обработку других элементов страницы до завершения выполнения скрипта.
        
- `<script async>`:
    
    - **Асинхронный сценарий**. Браузер загружает и выполняет его асинхронно.
        
    - Этот скрипт не блокирует обработку страницы, его загрузка и выполнение происходят параллельно с загрузкой остальных элементов страницы.
        
- `<script defer>`:
    
    - **Отложенный сценарий**. Браузер загружает его асинхронно, но выполняет только после полной загрузки страницы.
        
    - Этот скрипт также не блокирует обработку страницы, но гарантирует, что он выполнится после завершения построения DOM.

### Что делает атрибут loading (img, iframe, video)

**`loading="lazy"`:** Этот атрибут сообщает браузеру, что изображение можно загрузить позже, когда оно окажется в видимой области экрана.

### Что такое атрибут target? Какие значения он принимает?

Атрибут работает в связки с тегом ссылкой. По умолчанию при переходе по ссылке документ открывается в текущем окне, при необходимости это поведение можно изменить с помощью атрибута target.

- **target=”_blank”** загружает страницу в новое окно браузера.
- **target=”_self”** загружает страницу в текущем окне, значение по умолчанию.
- **target=”_parent”** - загружает страницу во фрейм родителя, если фреймов нет то ведет себя как self.
- **target=”_top”** - отменяет все фреймы и загружает страницу в текущем окне.


### Что такое srcset? Как работает srcset?

`srcset` - это HTML-атрибут, который позволяет адаптивно менять изображения в зависимости от условий отображения.

## Теги

### Что такое семантика? Какие семантические теги есть?
Семантические теги – это HTML-теги, которые помогают поисковым системам и веб-браузерам понимать структуру и содержание веб-страницы. Вот несколько примеров семантических тегов HTML5:

1. `<header>` - определяет верхнюю часть страницы или раздела.
    
2. `<nav>` - обозначает навигационное меню.
    
3. `<main>` - указывает на основной контент страницы.
    
4. `<article>` - используется для независимого, самодостаточного контента.
    
5. `<section>` - определяет раздел в документе.
    
6. `<aside>` - содержит дополнительную информацию или sidebar.
    
7. `<footer>` - обозначает нижнюю часть страницы или раздела.
    
8. `<figure>` - используется для группирования элементов, например, изображений с подписями.
    
9. `<figcaption>` - предоставляет подпись к элементу `<figure>`.
    

Эти теги делают код веб-страницы более понятным и улучшат SEO-оптимизацию.

-  `<strong>`
-  `<em>`

### Что такое  meta-тег с name="viewport"?

`meta`-тег с атрибутом `name="viewport"` используется для управления отображением веб-страницы на мобильных устройствах и других устройствах с различными размерами экранов.
### Что такое мета-тэги?

Мета-тэги – это элементы HTML-кода, которые добавляют информацию о веб-странице. Они находятся в секции `<head>` страницы и не отображаются напрямую для пользователей. Мета-тэги предоставляют поисковым системам и браузерам полезные данные, такие как описание страницы, ключевые слова и авторство.

```html
- <meta name="description" content="Описание страницы">
    
- <meta name="keywords" content="ключевое слово1, ключевое слово2">
    
- <meta name="author" content="Автор страницы">
```

Мета-тэги помогают улучшить SEO (поисковую оптимизацию) и делают веб-страницы более доступными и понятными для поисковых систем и других сервисов.
### Для чего нужен тег figure?

Тег `<figure>` используется в HTML для группировки медиа-контента, такого как изображения, иллюстрации, схемы или диаграммы, с их соответствующими подписями. Этот тег помогает улучшить структуру и семантику веб-страниц, делая контент более понятным для поисковых систем и удобным для пользователей.

```html
<figure>
  <img src="example.jpg" alt="Пример изображения">
  <figcaption>Подпись к изображению, объясняющая его содержимое.</figcaption>
</figure>
```
В данном примере тег `<figure>` группирует изображение и его подпись, заключённую в теге `<figcaption>`. Это помогает связать медиа-элемент с его пояснением и сделать контент более доступным и структурированным.
### Для чего используется элемент picture?
Элемент `<picture>` в HTML используется для предоставления браузеру нескольких версий одного и того же изображения, что позволяет выбрать наиболее подходящую в зависимости от условий отображения, таких как размер экрана, разрешение устройства или другие факторы. Это помогает улучшить производительность и адаптивность веб-страниц.
```html
<picture>
  <source srcset="image-large.jpg" media="(min-width: 800px)">
  <source srcset="image-medium.jpg" media="(min-width: 400px)">
  <img src="image-small.jpg" alt="Описание изображения">
</picture>
```


### Для чего используются тэги `<sub>` и `<sup>`?0

1) **Тэг** `<sub>` (subscript) используется для создания нижнего индекса.
2) **Тэг** `<sup>` (superscript) используется для создания верхнего индекса.

Эти теги относятся к текстовым тегам, они используется для выделения символов, а не слов, их применяют для указания единиц измерений и формул, а также для выделения специфических элементов.****

### Для чего нужен тег `<iframe>`? 
Тег `<iframe>` используется в HTML для внедрения содержимого одной веб-страницы на другую. Это позволяет отображать веб-страницу или другой HTML-контент внутри рамки на текущей странице. Тег `<iframe>` может быть полезен для включения внешних ресурсов, таких как видео, карты, формы или другой веб-контент.

## Прочее

### Какие минусы у Html

- SEO

