==JavaScript== — это высокоуровневый,  динамически типизированный язык программирования.

__Особенности JS:__

- ==Высокоуровневый== - означает, что он абстрагирует множество деталей, связанных с работой аппаратного обеспечения и операционной системы. 
- ==Интерпретируемый== - значит, что его код выполняется интерпретатором построчно. Это позволяет быстро тестировать и изменять код без необходимости его компиляции. Код JavaScript обычно выполняется интерпретаторами,  такие как V8 (Chrome, Node.js) и SpiderMonkey (Firefox).
- ==Динамически типизированный== - означает, что тип переменной определяется во время выполнения.
- ==Не явно тепизируемый== - переменные не требуют явного указания типов при объявлении.
- ==Слабая типизация== - означает, что язык автоматически преобразует данные между разными типами, когда это необходимо.
- ==Мультипарадигменный== - поддерживает несколько парадигм программирования(OOP, functional).


## Call, Apply, Bind

| Метод   | Описание                                                                                 | Аргументы                  |
| ------- | ---------------------------------------------------------------------------------------- | -------------------------- |
| `call`  | Вызывает функцию с указанным значением `this` и аргументами, переданными по отдельности. | `thisArg, arg1, arg2, ...` |
| `apply` | Вызывает функцию с указанным значением `this` и аргументами, переданными в виде массива. | `thisArg, [argsArray]`     |
| `bind`  | Создает новую функцию с указанным значением `this` и фиксированными аргументами.         | `thisArg, arg1, arg2, ...` |

### Call

Метод `call` вызывает функцию с указанным значением `this` и аргументами, переданными по отдельности.

```js
function greet(greeting, punctuation) {
    console.log(`${greeting}, ${this.name}${punctuation}`);
}

const person = { name: "Alice" };

greet.call(person, "Hello", "!"); // "Hello, Alice!"
```

### Apply

Метод `apply` аналогичен методу `call`, но аргументы передаются в виде массива.

```js
function greet(greeting, punctuation) {
    console.log(`${greeting}, ${this.name}${punctuation}`);
}

const person = { name: "Alice" };

greet.apply(person, ["Hello", "!"]); // "Hello, Alice!"
```

### Bind

Метод `bind` создает новую функцию, указывая значение `this` и, при необходимости, фиксируя некоторые или все аргументы. Эта новая функция может быть вызвана позже.

```js
function greet(greeting, punctuation) {
    console.log(`${greeting}, ${this.name}${punctuation}`);
}

const person = { name: "Alice" };

const boundGreet = greet.bind(person, "Hello");
boundGreet("!"); // "Hello, Alice!"
```

